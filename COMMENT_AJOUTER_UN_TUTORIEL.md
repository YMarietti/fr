# Comment ajouter/editer un exercice ?

Le répertoire `_docs` contient les différents exercices. Créez dans ce répertoire un nouveau répertoire contenant les documents Markdown et les images de votre exercice.

Les images peuvent être placées dans le sous-répertoire de `_docs` créé pour l'exercice ou bien mis dans le répertoire `assets/images` si les images sont réutilisables un peu partout dans le site.

> A noter: le dépôt https://gitlab.com/stm32python/assets contient les assets communs à tous les sites (fr, en ...)

Le document `_data/toc.yml` contient la description du sommaire (à gauche) du site. Il convient de compléter ce document avec les entrées vers votre tutoriel.

Avant d'effectuer un push vers le site central, vérifiez le bon fonctionnement de la génération depuis votre environnement local avec les commandes suivantes puis en ouvrant la page `http://127.0.0.1:4000/fr/`

```bash
git clone git@gitlab.com:stm32python/fr.git
bundle update
bundle install
bundle exec jekyll build
bundle exec jekyll serve
```

Une fois le push lancé, vérifiez l'état du pipeline CI/CD https://gitlab.com/stm32python/fr/pipelines

# Remarques
* Le document `_config.yml` contient les paramêtres du site utilisés pour la génération.
* Le répertoire `pages` contient les patrons des pages HTML générées.
* Le répertoire `_includes` contient les patrons des fragments HTML utilisés pour la génération des pages HTML.
* Les pages HTML sont générées dans le répertoire `_site`
* Le répertoire `assets` contient tous les _assets_ (`.css`, `.js`, `.svg`, `.png` ...) référencés dans les pages HTML générées.
