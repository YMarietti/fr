# TODOLIST

## Général
* Choix de la licence pour les codes sources (probablement BSD-3)
* Choix de la licence pour les documents (probablement une des CC https://creativecommons.org/licenses/?lang=fr-FR)
* lister les contributeurs dans `CONTRIBUTORS.md`
* ajouter des references Wikipedia dans le glossaire

## Tutoriel
* Guide d'installation Windows 10
* Tutoriel pour [Fritzing](https://fritzing.org/home/)
* Tutoriel pour [Jupyter](https://jupyter.org/)
* Tutoriel pour Matlab
* Tutoriel pour les cartes fille : MEMS, Teseo, [DC Motor Driver](https://air.imag.fr/index.php/File:MonsterMotoShield%2BSTNucleoF401.jpg), Stepper Driver ...
* Tutoriel Mini-Robot avec application de télécommande
    * accéléromêtre (choc) et magnetomêtre (correction de trajectoire) du [X-NUCLEO-IKS01A3](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html)
    * H-Bridge pour 2 moteurs DC
    * [Châssis Robotique 2WD](https://www.robotshop.com/eu/fr/chassis-robotique-2wd-pour-debutant.html)
* Tutoriel Station météo + Qualité de l'air
    * [X-NUCLEO-IKS01A3](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html)
    * [Weather Meter Kit](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html)
    * Grove Arduino shield
    * [RJ11 6-Pin Connector](https://www.sparkfun.com/products/132) x2
* Tutoriel G-Code pour le pilotage de plusieurs moteurs pas-à-pas (_steppers_ _motors_) NEMA.
* Ajout de la carte STM32WB55 à l'[IDE MBlock](https://ide.mblock.cc/?python#/devices) ([voir](https://www.mblock.cc/doc/en/developer-documentation/add-device-1.html))

## Firmware
* STM32WL55 (tutoriel avec TheThingsNetwork)

## Pages
* Générer les favicons à partir du logo
* Ajout d'un menu pour le changement de langue
* Ajout d'une grille pour la page d'accueil (Masonry https://masonry.desandro.com/)
* Ajout les tags aux pages et faire une page de tags 
* Ajout de SEO tags https://github.com/jekyll/jekyll-seo-tag
* [x] Ajouter le logo STM32Python
* Ajouter des permalinks dans les index des tutoriels
* Ajouter un site à propos de la vie privée (site.privacy dans _config.yml)
* Ajout du feed.xml ?

## Conventions d'écriture
* nom de fichiers et images en minuscule
* rédaction/correction des tutoriels à la seconde personne du pluriel "vous".