---
title: Tutoriel AppInventor
---
# Création d'une application avec AppInventor

<img src="images/Mit_app_inventor.png" width="200">

La carte IKS01A3 en complement de la Nucleo-WB55 du kit pédagogique STM32Python permet de fournir de nombreuses informations, comme par exemple la température.

La carte Nucleo-WB55 dispose de la connectivité BLE (Bluetooth Low Energy) et peut donc être interrogée ou pilotée à distance à l’aide d’un smartphone. L’objectif de ce tutoriel est de créer une application pour smartphone qui récupère puis affiche les mesures de capteurs (par exemple ceux d’un shield IKS01A3) connectés aux broches d’une carte Nucleo-WB55.
Pour ce faire, nous utiliserons l'outil MIT App Inventor qui permet de simplifier la création d'applications notamment grâce à une interface graphique similaire à celle de Scratch.

Afin que ce tutoriel fonctionne, il vous faut télécharger 3 fichiers (fournis par STMicroElectronics) et les copier sur la carte Nucleo-WB55 : 
* <a href="../../../assets/MITAppInventor/files_for_BLE/ble_advertising.py" download>ble_advertising.py</a>
* <a href="../../../assets/MITAppInventor/files_for_BLE/ble_sensor.py" download>ble_sensor.py</a>
* <a href="../../../assets/MITAppInventor/files_for_BLE/main.py" download>main.py</a> (il faut remplacer le main.py de la carte par ce main.py là)

## 1. Initilisation du projet

Dans un premier temps, nous allons créer un nouveau projet depuis l'application AppInventor et analyser l'interface de développement.

Vous pourrez accéder au site de **AppInventor** en[cliquant ici](https://appinventor.mit.edu/).

Pour démarrer un nouveau projet il suffit de cliquer sur *Create Apps!*.

<div align="center">
<img src="images/fenetre_1.png" width="700px">
</div>

Vous pourrez, à partir de ce moment-là, changer de langue (en haut à droite) et passer à la langue française : notre tutoriel est fait à partir du site en français.
Une fois votre compte créé vous pourrez cliquer sur *Commencer nouveau projet...*. Nous appellerons notre projet "MyApply".

L'interface de développement de AppInventor est séparée en 2 parties : la partie design et la partie code.

<div align="center">
<img src="images/fenetre_design.png" width="400px">
<img src="images/fenetre_code.png" width="400px">
</div>


Vous pourrez passer d'une fenêtre à l'autre en cliquant sur les boutons *Designer* et *Blocs* situés en haut à droite de l'écran

## 2. Design de l'écran d'accueil

Dans un premier temps nous allons travailler sur le design de notre application. Nous allons devoir déterminer quels sont éléments (boutons, label, listes ...) nécessaires.

Sur le côté gauche, vous disposez de plusieurs onglets contenant toutes les fonctionnalités que vous pouvez apporter à votre application.

Pour ajouter un item à notre écran il suffit de le faire glisser sur le dessin de l'écran de téléphone (la fenêtre *Interface*). Vous pouvez voir ici le menu prinicpal de notre application :

<div align="center">
 <img src="images/fenetre_home.png" width="500px">
</div>

La mise en place en place des items se fait sous forme d'arbre. Il est important d'identifier les différentes zones et les différentes fonctionnalités qu'elles vont contenir.

Ici, on peut voir que les items (la loupe et le texte) sont encadrés dans une zone. Ces zones sont aussi appelées des *dispositions*. Elles définissent un "cadre" dans lequel sont regroupées plusieurs fonctionnalités.

* Ouvrez donc l'onglet _Disposition_ sur le côté gauche.

Il faut à présent choisir un type d'arrangement pour nos items dans ces zones : arbitrairement nous choisirons un arrangement vertical.

* Sélectionnez _Arrangement vertical_ et faites le glisser sur l'écran du smartphone.

<div align="center"><img src="images/arrangement_vertical.png" width="700px"></div>

Pour pouvoir centrer notre arrangement vertical, nous avons cliqué sur *Screen1* (dans la partie *Composants*), puis nous avons défini l'arrangement horizontal et vertical à *Center*.


On peut voir sur l'écran, 2 objets différents :
* des textes : ce sont des _labels_, on peut les retrouver sous l'onglet _Interface Utilisateur_
* une image : cette image est en réalité un bouton (_Bouton_) que l'on peut retrouver sous l'onglet _Interface Utilisateur_ également.

Ces objets peuvent être améliorés en changeant leur couleur, ou leur taille par exemple, sous le menu qui se trouve à droite (*Propriétés*). C'est pour cela, par exemple, que le bouton du centre est représenté avec une image de loupe ou que certains textes sont en gras, ou plus gros. Vous pourrez les personnaliser à votre guise.

Faites donc glisser sur l'écran 3 _Labels_ et un _Bouton_ à l'intérieur de votre _Arrangement vertical_. Vous pouvez changer le texte de ces composants dans la partie _Texte_ des _Properties_.

Notre application va servir à se mettre en relation avec la carte via BLE. Une fois la géolocalisation et le Bluetooth de votre téléphone activés, nous allons analyser notre environnement et sélectionner parmi la liste des appareils disponibles, celui auquel nous voulons nous connecter. Nous avons donc besoin d'un *Sélectionneur de liste*. Celui-ci se trouve dans l'onglet *Interface Utilisateur*.

**Attention :** afin d'obtenir le design souhaité, vous devez rendre cette liste non visible en décochant le champ *Visible* dans ses *Propriétés*.

Entre l'*Interface* et les *Propriétés* vous retrouverez une partie *Composants* qui vous montre l'arbre des widgets que vous avez ajoutés à votre application. Voici à quoi votre arbre devrait ressembler :

<div align="center">
<img src="images/component.png" width="300px">
</div>

Vous pouvez cliquer sur *Renommer* pour renommer vos composants comme sur l'image ci-dessus.

Une dernière partie très importante est la capacité de notre application à se connecter en bluetooth. Dans notre cas c'est une connexion BLE qui n'est pas disponible dans les fonctionnalités de base de AppInventor.

En dessous de la partie *Palette* vous trouverez un onglet *Extension*. Il faut importer le fichier <a href="../../../assets/MITAppInventor/connexion_BLE.aix"
 download>connexion_BLE</a>

Une fois importée vous devez la faire glisser dans l'_Interface_. Vous la verrez apparaitre sous le smartphone dans la catégorie _Non visible components_.

De même pour pouvoir gérer différentes alertes comme la détection d'appareil vous aurez besoin d'un _Notificateur_ dans la catégorie _Interface Utilisateur_.

Vous devriez maintenant obtenir le résultat présenté au début de cette partie.

## 3. Mise en place de la connexion

Pour faire notre application nous allons devoir associer un comportement à ces composants, suivant les actions de l'utilisateur.

Basculez donc maitenant dans la partie *Blocs*

<div align="center">
<img align="center" src="images/fenetre_code.png" width="600px">
</div>

La partie *Blocs* contient tout le "code" dont vous aurez besoin.

Vous retrouvez ici toutes les bases de la programmation, avec les opérateurs booléens (vrai, faux), les synthaxes de boucles, etc. Chaque catégorie est symbolisée par une couleur différente comme vous pouvez le voir dans le panneau à gauche de l'écran. Nous pouvons également modifier ou appeler des instructions sur nos différents composants (créés dans la partie *Design*) en cliquant sur le composant à modifier et en utilisant les différents méthodes proposées.

### Démarrage de l'application

La première étape est d'initialiser notre écran d'accueil, c'est-à-dire de définir les comportements qui se produisent à l'ouverture de l'application.
Pour cela, il existe dans le composant _Screen1_ la structure `quand Screen1 initialise`. Faites glisser sur l'écran cette structure.

Pour nous aider à visualiser l'état de notre connexion bluetooth nous choisissons de changer le fond du label `Etat_connexion ` suivant l'état de la connexion :

* <span style="color: #FF0000">Rouge</span> : Pas de connexion
* <span style="color: #FFC800">Orange</span> : Connexion en cours
* <span style="color: #00FF00">Vert</span> : Connexion réussie

Pour changer la structure d'un label il faut utiliser la structure `mettre ... à`.

En cliquant sur le composant `Etat_connexion` nous pouvons donc sélectionner la structure `mettre Etat_connexion Couleur de fond à` et choisir la couleur adéquate dans la partie _Couleurs_ (ici le rouge). Faites glisser ce composant dans le bloc `quand Screen1 initialise`.

De même en l'abscence de connexion, le label `Etat_connexion` doit contenir le texte "Aucune connexion".

Donc nous pouvons également nous servir de`mettre Etat_connexion Text à` en y ajoutant un champ d'écriture dans _Texte_.

Vous devriez obtenir ceci :

<div align="center">
<img align="center" src="images/cnx_1.png" width="600px">
</div>

### Initilialisation du bouton connexion

Nous allons maintenant gérer ce qu'il se passe quand on appuie sur le bouton *Connexion* (`quand Connexion Clic`).

Donc sélectionnez ce composant là et ajoutez-le sur l'écran de code.

Celui-ci doit permettre d'activer la recherche d'appareil (`appeler BluetoothLE1 StartScanning`).

Imbriquez ce code là dans le précédent.

Pour aider l'utilisateur à connaitre l'état de l'application, nous utiliserons le *Notificateur* pour préciser l'action en cours.

Rajoutez enfin le bloc `appeler Notificateur1 Afficher Alerte notice` + _Texte_ : "Recherche BLE en cours .." `

<div align="center">
<img align="center" src="images/cnx_2.png" width="600px">
</div>

### Recherche de périphériques

Nous devons maintenant définir ce que l'on doit faire quand des appareils ont été trouvés.

Faites glisser le bloc `quand BluetoothLE1 DeviceFound` dans la fenêtre.

Il faut dans un premier temps arrêter la recherche de périphériques.

Imbriquez le bloc `appeler BluetoothLE1 StopScanning` à l'intérieur du précédent.

Dans le champ *DeviceFound* de notre BLE se trouve la liste des périphériques disponibles. Nous allons donc mettre à jour notre *Liste_BLE* avec les éléments trouvés par notre BLE qui sont caractérisés par leur nom (donc par une chaîne de caractère) :

Ajoutez le bloc `mettre List_BLE Éléments de la chaine à BluetoothLE1 DeviceList `.

Notre *label* `Etat_connexion` a aussi changé. Celui ci va maintenant nous donner le nombre de BLE trouvés qui est équivalent à la taille de la liste des éléments de *list_BLE_Devices*.

Pour cela, ajoutez le bloc `mettre Etat_connexion Texte à`.

**Attention :** ici, vous voulez rajouter du texte avec une information contenue dans un composant. Vous aurez donc besoin de joindre ces deux éléments.

Pour cela, utilisez la structure `joint` dans l'onglet _Texte_

Vous voulez donc joindre un texte et la longueur de la liste d'appareils trouvés.

Pour cela, ajoutez un champ `Texte` contenant "Nombre de BLE trouvés :" et imbriquez le à la première partie du bloc `joint`.

Pour la deuxième partie de cette structure, nous allons utiliser la structure `taille de la liste` (dans _Listes_) et y ajouter la liste souhaitée (celle dont nous souhaitons connaître la taille) : `Liste_BLE Éléments`.

Une fois ces changements effectués nous pouvons afficher la liste des appareils trouvés en ajoutant la structure `appeler Liste_BLE ouvrir`.

<div align="center">
<img align="center" src="images/cnx_3.png" width="600px">
</div>

### Choix du périphérique

Une fois la liste des appareils disponibles affichée, nous allons devoir sélectionner celui qui nous intéresse.

#### Initialisation de l'adresse

L'adresse de ce périphérique sera stockée dans une variable (onglet orange *Variables* dans le panneau latéral). Il est donc important avant tout d'initialiser cette variable, de dire qu'elle existe et lui donner un nom.

Pour cela, ajoutez le bloc `initialise global _nom_ à` et modifiez le nom pour que notre variable s'appelle _Adresse_.

Comme cette adresse n'a encore aucune valeur nous pouvons lui donner un champ de texte vide.

Imbriquez donc un champ _Texte_ vide à cette structure.

<div align="center">
<img align="center" src="images/cnx_4.png" width="600px">
</div>

#### Récupération de l'adresse

Nous allons donc traiter ce qu'il se passe après avoir choisi votre appareil.

Pour cela, nous allons utiliser la structure `quand Liste_BLE Après prise`.

Dans un premier temps nous allons changer la valeur de notre variable *Adresse* en lui affectant celle sélectionnée dans la liste.

Imbriquez donc le bloc `mettre ... à` (qui se trouve dans la catégorie _Variables_ puisque nous voulons modifier la valeur d'_Adresse_). Puis utiisez la structure `Liste_BLE Sélection` pour mettre à jour notre variable avec l'appareil sélectionné depuis la liste.

#### Changement d'indicateur

Nous allons ensuite changer notre témoin de connexion pour informer l'utilisateur du changement d'état de l'application.

Comme nous l'avons fait précedemment, nous allons changer le `Texte` du label `Etat_connexion`.

Comme précédemment nous allons vouloir signifier à quel périphérique nous souhaitons nous connecter. Pour cela, nous allons utiliser des blocs `joint` .

Ajoutez donc un bloc joint qui sera composé d'un _Texte_ : "Connexion à ", suivi de l'adresse de notre périphérique stockée dans notre variable _Adresse_ récupérée grâce à la structure `obtenir global Adresse`.

Or, la connexion n'étant pas encore terminée, nous allons préciser qu'elle est en cours.

Rajoutez pour cela, une structure `joint` qui associera le contenu de notre variable _Adresse_ à un _Texte_ contenant " en cours". 

Nous changerons ensuite la couleur du fond en la passant à jaune.

Utilisez donc la structure `mettre Etat_connexion Couleur de fond à` et y clipser la couleur _Orange_.

#### Connexion au périphérique

Une fois la carte sélectionnée vous devez établir la connexion entre celle-ci et l'application.

Pour cela nous utiliserons la structure ` appeler BluetoothLE1 Se connecter index `. 

L'index représente le numéro du périphérique dans notre liste d'appareils disponibles. C'est donc celui ayant le nom de notre *Adresse* dans les éléments de la List_BLE.

Il faut donc ajouter la strucure `index dans la liste` qui combinera l'adresse du périphérique, donc notre variable _Adresse_ (récupérée grâce au bloc `obtenir`) et `Liste_BLE Éléments` .

<div align="center">
<img align="center" src="images/cnx_5.png" width="600px">
</div>

Vous devriez finalement obtenir un code comme celui ci :

<div align="center">
<img align="center" src="images/code_cnx.png" width="600px">
</div>


## 4. Design de l'écran de contrôle

Comme pour l’écran de connexion, nous devons définir quels composants seront nécessaires à l’allumage de la LED et la prise de température.

Nous souhaitons obtenir le résultat suivant :

<div align="center">
<img align="center" src="images/deuxieme_ecran.png" width="300px">
</div>

Pour cela, nous allons avoir besoin d’un label pour signifier à l’utilisateur que la connexion est établie, un bouton switch qui nous permettra d’allumer et d’éteindre la LED, un label pour afficher « Température : » (par pur esthétisme) ainsi qu’un dernier label qui servira à afficher la température courante.

Nous allons néanmoins réutiliser le label *Etat_connection* créé précédemment pour afficher la réussite de la connexion avec la carte.

Nous allons utiliser un _Arrangement vertical_ pour stocker tous ces éléments (hormis le label _Etat_connexion_), que nous allons ajouter sous l'arrangement _Arrangement_connexion_. 

Nous allons maintenant glisser un _Switch_ dans cet _Arrangement vertical_, puis y rajouter un _Arrangement horizontal_.

Nous allons insérer deux _Labels_ dans ce dernier : l’un contenant "Température : " et l’autre contenant la température en temps réel. 

De plus, nous allons ajouter un _Bouton_ pour nous déconnecter de la carte.

Après avoir rajouté ces éléments, vous devriez obtenir ceci :

<div align="center"> <img align="center" src="images/component2.png" width="700px"> </div>

 **Attention :** Ces éléments doivent être invisibles tant que vous n'êtes pas connecté à une carte. Vous allez donc cliquer sur l'arrangement *Fonctionnalités* et décocher le champ *Visible* dans les *Propriétés*. De même, pour le bouton *Déconnexion*. Ce qui vous donnera un résultat suivant :

<div align="center"> <img align="center" src="images/invisible.png" width="700px"> </div>

Evidemment, nous ne pouvons pas supprimer les éléments nécessaires à la connexion. Donc, pour obtenir l’apparence que l’on souhaite, il nous faut réflechir à ce que l’on doit rendre invisible. En effet, lorsque la connexion sera établie, nous n’aurons plus besoin des labels *Conseil_1* et *Conseil_2* ainsi que du bouton *Connection*.


## 5. Établissement de la connexion et changement d'écran

 Maintenant que cette nouvelle page est créée nous allons programmer ce changement d'écran.
 Ce changement se fera une fois que la connexion est définitivement établie avec le périphérique.

 Ajoutez donc un bloc `quand BluetoothLE1 Connected`. 

Il faut donc cacher les éléments courants (sauf le label *Etat_connexion*) et afficher les éléments de la nouvelle page.

#### Changement de page

Nous voulons donc rendre invisible les éléments *Conseil_1*, *Conseil_2* et *Connexion*.

 Pour chacun de ces composants, utilisez la directive `mettre ... Visible à` et y imbriquez le champ *Logique* `faux`.

Inversement, nous voulons voir apparaître les éléments liés aux *Fonctionnalités* et le bouton *Déconnexion*.

Modifiez donc le champ _Visible_ de ces éléments là mais cette fois, imbriquez la valeur logique à `vrai`.

Ces éléments vont donc devenir visibles.

Entre ces deux changements d'états nous allons changer le témoin de connexion `Etat_connexion` en passant son fond à _Vert_ et son message à "Connexion à .. réussie" comme nous l'avons fait précedemment.

#### Paramétrage de la carte

 Nous allons ensuite paramétrer des `UUID`. Les UUID sont des clés uniques permettant l'identification entre deux objets BLE. Ce mécanisme est propre à la connexion BLE.
 Nous allons devoir en définir un certain nombre, un pour mettre en lien le STM32 et le téléphone et d'autres pour gérer l'information (LED, température et autres capteurs). Dans notre cas, nous allons utiliser un capteur de température et manipuler des LEDs donc nous utiliserons 2 UUID supplémentaires, soit 3 en tout.

Il va donc falloir définir 3 variables au tout début du code, comme nous l'avions fait pour `global Adresse`.

Ces 3 variables s'appellerons `Service_UUID` et `SerialPORT_Temp_UUID` (pour le Température) et `SerialPORT_LED_UUID` (pour les LED). Ces UUID sont des paramètres propres à la carte que vous trouverez dans le code C associé. Les valeurs sont donc les suivantes :

<div align="center">
<img align="center" src="images/UUID.png" width="500px">
</div>

 Le traitement de la température est un peu spécial. En effet, il faut préciser dans le programme que nous souhaitons recevoir des floats provenant de l'UUID *SerialPORT_Temp_UUID*.

 Nous allons donc rajouter la structure `appeler BluetoothLE1 RegisterForFloats` qui va indiquer que l'on souhaite recevoir des _Floats_ (des réels).

 Nous allons maintenant associer au champ _serviceUuid_ celui que nous avons défini précédemment grâce à la structure `obtenir global Service_UUID`.

 Ensuite il faut indiquer quel UUID particulier doit être utilisé (donc celui de la température) : associez donc le champ _characteristicUuid_ à notre varible _SerialPORT_Temp_UUID_.

 Enfin, nous allons juste signifier que nous souhaitons avoir les nombres en entier donc mettre à _faux_ le champ _shortFloat_. 

 Finalement, vous devriez obtenir le code suivant :

<div align="center">
<img align="center" src="images/code_cnx2.png" width="600px">
</div>

## 6. Implémentation des capteurs

Nous devons maintenant associer le code qui prend la température et celui qui allume la LED aux composants correspondants.


### LED

Etant donné qu'on utilise un *Switch* pour allumer et éteindre la LED, nous allons être attentifs au fait que le bouton change ou pas.

 Utilisez donc un bloc `quand LED Changé`.


Dans ce bloc, nous allons distinguer deux cas : si on veut allumer la LED et si on veut l'éteindre. Nous allons donc tester si le *Switch* est enfoncé ou pas et faire un traitement différent en fonction de l'état du *Switch*.

 Ajoutez la structure `si ... alors ... sinon` se trouvant de l'onglet _Contrôle_

Ce que nous allons tester est l'état de notre *Switch*.

 Imbriquez le bloc `LED On` au _si_ du bloc précédent.

<div align="center"><img align="center" src="images/led.png" width="600px"></div>

#### Allumer la LED

Si le *Switch* est enfoncé, nous allons signifier à la carte que la LED doit être allumée maintenant.

Pour cela, nous allons écrire à la carte grâce à la structure `appeler BluetoothLE1 WriteBytes`. 

Cette fonction va permettre de désigner à quel capteur parle la carte.

Comme auparavant, la valeur du _service UUID _ est contenue dans notre variable globale _Service_UUID _ (récupérée grâce à la fonction `obtenir global Service_UUID`).

La valeur du _characteristicUUID_ est elle contenue dans la variable _SerialPORT_LED_UUID_ que l'on obtient comme précédemment. 

On définit le champ _signed_ à `vrai` (on aurait pu le mettre à faux, cela n'a pas d'incidence sur notre code, cela indique seulement à la carte si les valeurs qu'on lui transmet peuvent être négatives ou pas). 

Nous sommes dans le cas où la LED doit être allumée donc on définit la valeur _value_ à `1` (champ `0` dans la catégorie _Maths_, que l'on modifie à 1).

<div align="center"><img align="center" src="images/led_1.png" width="600px"></div>

####  Éteindre la LED

Inversement, lorsque l'on veut éteindre la LED, on va faire la même chose mais écrire la valeur 0.

Faites un clic-droit sur le bloc `appeler BluetoothLE1 WriteBytes` et cliquez sur _Dupliquer_. Imbriquez ce bloc dupliqué dans le _else_ et modifiez seulement la valeur `1` par la valeur `0`. 

<div align="center">
<img align="center" src="images/led_2.png" width="600px">
</div>


### Capteur Température

Le capteur de température va mesurer une valeur et l'envoyer à la carte. Nous devons récupérer cette valeur et l'afficher à l'écran.

Pour cela nous avons besoin de la structure `quand BluetoothLE1 FloatsReceived` qui indique que le capteur de température a reçu des données.

Nous allons maintenant modifier notre label _Température_ en utilisant la fonction `mettre Température Texte à' à laquelle on associera la valeur contenue dans la variable _floatValues_ grâce à au bloc `obtenir floatValues` (_floatValues_ contient les valeurs reçues par la carte grâce au capteur de température).

<div align="center">
<img align="center" src="images/temperature.png" width="500px">
</div>

Vous devriez obtenir le code suivant :

<div align="center">
<img align="center" src="images/led_tmp.png" width="500px">
</div>


## 7. Déconnexion

Une fois que l'utilisateur clique sur le bouton _Déconnexion_, nous devons déconnecter l'application du périphérique et revenir à la page d'accueil.
Comme pour le bouton *Connexion*, la déconnexion se fait en 2 temps : un bloc pour l'appui sur le bouton et un bloc pour la déconnexion du BLE.

Dans un premier temps il faut utiliser un bloc `quand Déconnexion Clic`. 

Dans ce bloc on appellera simplement la fonction ` appeler BluetoothLE1 Déconnecter`. 

<div align="center">
<img align="center" src="images/dcnx_1.png" width="500px">
</div>

Le deuxième bloc sera donc construit autour de la structure `quand BluetoothLE1 Disconnected `. 

Dans ce bloc, nous allons :
 Changer la couleur de fond de notre témoin de connexion à rouge
 Changer le message du témoin à "Aucun appareil connecté"
 Rendre visible les _Labels_ `Advice`, `Advice2` et le _Bouton_ `Connexion`
 Rendre invisible le _Bouton_ `Déconnexion` et l'_Arrangement vertical_ `Fonctionnalités`.

<div align="center">
<img align="center" src="images/dcnx_2.png" width="500px">
</div>
