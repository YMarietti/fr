---
title: AppInventor
description: Comment utiliser le kit STM32 MicroPython avec MIT App Inventor
---
# Comment utiliser le kit STM32 MicroPython avec MIT App Inventor ?

## MIT App Inventor

<img src="images/Mit_app_inventor.png" width="200" alt="MIT App Inventor">

[MIT App Inventor](http://appinventor.mit.edu/) est un environnement de programmation visuel intuitif qui permet à tout le monde - même aux enfants - de créer des applications entièrement fonctionnelles pour smartphones et tablettes. Le langage de programmation utilisé est [Scratch](https://scratch.mit.edu/).

## Sommaire

Vous trouverez dans cette partie les exercices vous permettant de créer des applications MIT App Inventor pour smartphones et tablettes afin de dialoguer avec votre carte STM32 par l'intermédiaire de votre smartphone ou votre tablette.

 - [Création de l'application avec MIT App Inventor](application)
 - Programmation d'un mini-robot et contrôle de ses moteurs DC avec MIT App Inventor depuis un Smartphone (bientôt)
