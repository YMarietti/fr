---
title: Communication en BLE avec la carte depuis un Smartphone  
description: Communication en BLE avec la carte depuis un smartphone

---
# Communication en BLE avec la carte depuis un Smartphone 





Dans cette partie comment communiquer en Bluetooth Low Power avec l’application STBLESensor et la carte de développement WB55. 


## Installation de ST BLE Sensor sur votre smartphone

Installez sur votre smartphone, l’application ST BLE Sensor sur [Google Play](https://play.google.com/store/apps/details?id=com.st.bluems) ou [IOS Store](https://apps.apple.com/it/app/st-ble-sensor/id993670214)

![image](images/stmblesensorapp.png) ![Android](images/stmblesensorapp-qr-android.png)   ![iOS](images/stmblesensorapp-qr-ios.png)

Voici la description complète des différents services proposés par l’application STBLESensor :

[https://github.com/STMicroelectronics/STBlueMS_Android](https://github.com/STMicroelectronics/STBlueMS_Android)


## Communication BLE en MicroPython

Pour communiquer en Bluetooth Low Energy avec micropython, il faudra inclure 2 nouveaux fichiers dans le répertoire du disque usb “PYBFLASH” :
1. [ble_advertising.py](../../assets/Script/BLE/ble_advertising.py) (Fichier d’aide à la création du message d’advertissing)
2. [ble_sensor.py](../../assets/Script/BLE/ble_sensor.py)  (Classe permettant la gestion de la connexion BLE)
*   Il faudra télécharger les scripts nécessaires à cet exemple [ici](../../assets/Script/BLE/Script_BLE.zip)

Grâce au fichier ble_sensor.py, nous allons pouvoir créer un objet BLE ayant 1 service et 2 caractéristiques.

 

_C’est ce fichier qu’il faudra modifier pour changer le profil BLE, si besoin._

Une fois le script lancé, le kit de développement WB55 se met à émettre des trames BLE, appelé “advertising”. Ces messages permettent d’identifier l’objet Bluetooth et de signifier que le périphérique est prêt à être connecté. 

Le nom du périphérique est : “WB55-MPY”.Nous allons vérifier avec l’application smartphone si la carte WB55 est en émission bluetooth . 


## Utilisation

Lancez l’application STBLESensor sur votre SmartPhone :

![image](images/app1.png)


Appuyez ensuite sur l’icône loupe pour afficher les périphériques BLE environnant :

![image](images/app2.png)



Dans cette exemple, le profil BLE que nous avons choisi nous permet de simuler un thermomètre et d’allumer ou d’éteindre une LED. La valeur du thermomètre est générée aléatoirement toutes les secondes. 



Connectez-vous à la carte de développement en appuyant sur “WB55-MPY”:


![image](images/app3.png)


_La LED bleue de la carte WB55 doit s’allumer lorsqu’elle est connectée à l’application._

Nous pouvons observer, sur cette écran, l’évolution aléatoire de la température entre 0 et 100,0 °C.

Il est possible d’afficher la température en mode graphique. 

Pour cela appuyez sur le bouton menu ![image](images/app-menu.png) :

![image](images/app4.png)

Appuyez maintenant sur ![image](images/app-plot.png) :

![image](images/app5.png)

Pour afficher le graphique, appuyez sur ![image](images/app-play.png) :

![image](images/app6.png)

Vous pouvez utiliser le bouton ![image](images/app-options.png) pour modifier les options du graphique, comme la taille de l’axe X ou l’activation du changement automatique de l’échelle en Y.


Nous allons maintenant étudier l'envoi d'une information depuis le SmartPhone vers la plateforme WB55. 
Pour cela nous utilisons l’application pour allumer ou éteindre une la LED rouge du kit de développement. 


Pour cela appuyez ur le bouton menu ![image](images/app-menu.png) :

![image](images/app7.png)

Choisissez maintenant l’option ![image](images/app-switch.png) :

![image](images/app8.png)

Vous pouvez sur cet écran piloter la LED Rouge du kit de développement. 
