---
title: Exercices basiques avec MicroPython
description: Exercices basiques avec MicroPython

---
# Exercices basiques avec MicroPython

## Récupérer l’état d’un Bouton (GPIO) (Lecture de l’état d’un bouton)

Il existe 3 boutons SW1, SW2, SW3 disponibles pour le développeur : 

![image](images/boutons.png)

Nous allons voir dans cette sous partie comment initialiser une Pin en mode “Entrée” et afficher un message lors de l’appui sur un des 3 boutons en utilisant “**pyb.Pin”**. 

Nous utiliserons la méthode dite de “pulling” afin de demander au système MicroPython l’état de la Pin (1 ou 0). 

Pour des raisons électroniques, l’état de la Pin au repos, bouton relâché, est équivalent à 1 alors que l’état lors d’un appui bouton est 0.

*   Ouvrez l’éditeur de script et éditez le fichier main.py : 

```python
import pyb # Librairie de MicroPython permettant les accès aux périphériques (GPIO, LED, etc)
import time # Librairie permettant de faire des pauses systèmes
print("Les GPIO avec MicroPython c'est facile")
# Initialisation des Pin d'entrées (SW1, SW2, SW3)
sw1 = pyb.Pin('SW1', pyb.Pin.IN)
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw2 = pyb.Pin('SW2', pyb.Pin.IN)
sw2.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw3 = pyb.Pin('SW3', pyb.Pin.IN)
sw3.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
# Initialisation des variables
ancienne_valeur_sw1 = 0
ancienne_valeur_sw2 = 0
ancienne_valeur_sw3 = 0
while 1: # Création d'une boucle infinie
	# le Système s'endorts pendant 300ms
	time.sleep_ms(300)
	#Récupération de l'état des Boutons 1,2,3
	valeur_sw1 = sw1.value()
	valeur_sw2 = sw2.value()
	valeur_sw3 = sw3.value()
	#L'état courant est il différent de l'état précédent ?
	if valeur_sw1 != ancienne_valeur_sw1:
    	    if valeur_sw1 == 0:
        	        print("Le bouton 1 (SW1) est appuyé")
    	    else:
        	        print("Le bouton 1 (SW1) est relâché")
    	    ancienne_valeur_sw1 = valeur_sw1
	if valeur_sw2 != ancienne_valeur_sw2:
    	    if valeur_sw2 == 0:
        	        print("Le bouton 2 (SW2) est appuyé")
    	    else:
        	        print("Le bouton 2 (SW2) est relâché")
    	    ancienne_valeur_sw2 = valeur_sw2
	if valeur_sw3 != ancienne_valeur_sw3:
            if valeur_sw3 == 0:
        	       print("Le bouton 3 (SW3) est appuyé")
    	    else:
        	       print("Le bouton 3 (SW3) est relâché")
    	    ancienne_valeur_sw3 = valeur_sw3
```


*   Enregistrez le script main.py (CTRL + S), puis redémarrez la carte (CTRL+D). 

Lorsque vous appuyez sur un des 3 boutons (SW1, SW2, SW3), des messages dans la console vous indiquent l’état des boutons.

## Changer l’état d’une Sortie (GPIO) (Allumer une LED)

L’objectif est maintenant d’allumer les LEDs sur le kit de développement :

![image](images/leds.png)


Voici l’organisation des LEDs par couleur et numéro: 



1. LED 1 : Bleu
2. LED 2 : Vert
3. LED 3 : Rouge

Sous MicroPython, le module **pyb.LED** permet de gérer les LEDs très simplement. 

Dans cette partie, nous voulons réaliser un “chenillard”, cet exercice consiste à allumer puis éteindre les LEDs les unes après les autres, de façon cyclique. 

*   En utilisant la méthode précédente et le script suivant, réaliser le chenillard avec MicroPython : 

```python
import pyb
import time
print("Les LEDs avec MicroPython c'est facile")
# Initialisation des LEDs (LED_1, LED_2, LED_3)
led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)
# Initialisation du compteur de LED
compteur_de_led = 0
while 1: # Création d'une boucle infinie
    if compteur_de_led == 0:
        led_bleu.on()
        led_rouge.off()
        led_vert.off()
    elif compteur_de_led == 1:
        led_bleu.off()
        led_vert.on()
        led_rouge.off()
    else:
        led_bleu.off()
        led_vert.off()
        led_rouge.on()
    # On veut allumer la prochaine LED à la prochaine itération de la boucle
    compteur_de_led = compteur_de_led + 1
    if compteur_de_led > 2:
        compteur_de_led = 0
    time.sleep_ms(500) # le Système s'endorts pendant 500ms
```

## Lecture d’une valeur analogique (ADC)

Nous aimerions maintenant convertir la valeur analogique (0-3.3V) d’un signal en valeur numérique (0-4095). 

Vous pourrez brancher sur A0-5 une entrée renvoyant une tension comprise entre 0 et +3.3V (éventuellement variable au cours du temps). Ce connecteur est directement branché à l’ADC du microcontrôleur. Le signal peut donc être converti en signal numérique :

![image](images/adc.png)


Pour cette démonstration nous utiliserons un potentiomètre (10 KOhm) du commerce que nous branchons sur A0, comme cela : 

![image](images/potentiometre.png)

N’importe quel référence de potentiomètre 10 KOhm fonctionne pour la démonstration. Voici une référence : 

[PTV09A-4020F-B103](https://www.mouser.fr/ProductDetail/Bourns/PTV09A-4020F-B103?qs=sGAEpiMZZMtC25l1F4XBU1xwXnrUt%2FuoeIXuGADl09o%3D)  : ![image](images/potentiometre2.png)

Avec l’aide du script suivant, utilisez la fonction pyb.ADC pour interagir avec l’ADC du STM32 :

```python
import pyb
import time
print("L'ADC avec MicroPython c'est facile")
# Initialisation de l'ADC sur la Pin A0
adc_A0 = pyb.ADC(pyb.Pin('A0'))
while 1:
    valeur_numérique = adc_A0.read()
    # Il faut maintenant convertir la valeur numérique par rapport à la tension de référence (3.3V) et
    # le nombre de bits du convertisseur (12 bits - 4096 valeurs)
    valeur_analogique = (valeur_numérique * 3.3) / 4095
    print("La valeur de la tension est :", valeur_analogique, "V")
    # le Système s'endorts pendant 500ms
    time.sleep_ms(500)
```

Vous pouvez lancer le terminal Putty et observer la valeur en Volts qui évolue, toutes les 500ms, lorsque vous tournez le potentiomètre.

## Affichage sur un écran LCD OLED (I2C)

Il est très facile d’utiliser un écran OLED avec MicroPython pour afficher des messages. Nous verrons dans cet exemple comment brancher l’écran LCD en I2C, puis comment piloter l’écran pour envoyer des messages avec MicroPython. 

Pour l'exemple, nous utiliserons l’écran monochrome OLED, 192 x 32 pixels, de Adafruit, cependant tous les écrans intégrant le driver SSD1306 sont compatibles.

Voici comment brancher l’écran OLED : 
* D15=SCL
* D14=SDA

![image](images/oled.png)



Nous aurons besoin du fichier ssd1306.py, que vous pourrez télécharger à cette adresse : 

[ssd1306.py](../../assets/Script/Ecran/ssd1306.py)

Lorsque vous aurez terminé le téléchargement, il faudra transférer le fichier dans le répertoire du périphérique PYBLASH. 

*   Editez maintenant le srcipt main.py : 

```python
from machine import Pin, I2C
import ssd1306
from time import sleep

#Initialisation du périphérique I2C
i2c = I2C(scl=Pin('SCL'), sda=Pin('SDA'), freq=100000)

#Paramétrage des caractéristiques de l'écran
largeur_ecran_oled = 128
longueur_ecran_oled = 32
oled = ssd1306.SSD1306_I2C(largeur_ecran_oled, longueur_ecran_oled, i2c)

#Envoi du texte à afficher sur l'écran OLED
oled.text('MicroPython OLED!', 0, 0)
oled.text('    I2C   ', 0, 10)
oled.text('Trop facile !!!', 0, 20)
oled.show()
```
## Utilisation du BLE (Bluetooth Low Energy)
Dans cette partie nous allons voir comment communiquer en Bluetooth Low Power avec l’application STBLESensor et la carte de développement WB55. 

### Installation de ST BLE Sensor sur votre smartphone

Installez sur votre smartphone, l’application ST BLE Sensor sur [Google Play](https://play.google.com/store/apps/details?id=com.st.bluems) ou [IOS Store](https://apps.apple.com/it/app/st-ble-sensor/id993670214)

![image](images/stmblesensorapp.png) ![Android](images/stmblesensorapp-qr-android.png)   ![iOS](images/stmblesensorapp-qr-ios.png)

Voici la description complète des différents services proposés par l’application STBLESensor :

[https://github.com/STMicroelectronics/STBlueMS_Android](https://github.com/STMicroelectronics/STBlueMS_Android)

### Communication BLE en MicroPython
Pour communiquer en Bluetooth Low Energy avec micropython, il faudra inclure 2 nouveaux fichiers dans le répertoire du disque usb “PYBFLASH” :
1. [ble_advertising.py](../../assets/Script/BLE/ble_advertising.py) (Fichier d’aide à la création du message d’advertissing)
2. [ble_sensor.py](../../assets/Script/BLE/ble_sensor.py)  (Classe permettant la gestion de la connexion BLE)
Il faudra télécharger les scripts nécessaires à cet exemple [ici](../../assets/Script/BLE)

Grâce au fichier ble_sensor.py, nous allons pouvoir créer un objet BLE ayant 1 service et 2 caractéristiques.

_C’est ce fichier qu’il faudra modifier pour changer le profil BLE, si besoin._

Une fois le script lancé, le kit de développement WB55 se met à émettre des trames BLE, appelé “advertising”. Ces messages permettent d’identifier l’objet Bluetooth et de signifier que le périphérique est prêt à être connecté.

Le nom du périphérique est : “WB55-MPY”.Nous allons vérifier avec l’application smartphone si la carte WB55 est en émission bluetooth .

### Utilisation
Lancez l’application STBLESensor sur votre SmartPhone :

![image](images/app1.png)

* Appuyez ensuite sur l’icône loupe pour afficher les périphériques BLE environnants :

![image](images/app2.png)

Dans cette exemple, le profil BLE que nous avons choisi nous permet de simuler un thermomètre et d’allumer ou d’éteindre une LED. La valeur du thermomètre est générée aléatoirement toutes les secondes.

* Connectez-vous à la carte de développement en appuyant sur “WB55-MPY”:

![image](images/app3.png)

La LED bleue de la carte WB55 doit s’allumer lorsqu’elle est connectée à l’application.
Nous pouvons observer, sur cet écran, l’évolution aléatoire de la température entre 0 et 100,0 °C.
Il est possible d’afficher la température en mode graphique.

* Pour cela appuyez sur le bouton menu ![image](images/app-menu.png):

![image](images/app4.png)

* Appuyez maintenant sur ![image](images/app-plot.png):

![image](images/app5.png)

* Pour afficher le graphique, appuyez sur ![image](images/app-play.png):

![image](images/app6.png)

Vous pouvez utiliser le bouton ![image](images/app-options.png) pour modifier les options du graphique, comme la taille de l’axe X ou l’activation du changement automatique de l’échelle en Y.

Nous allons maintenant étudier l'envoi d'une information depuis le SmartPhone vers la plateforme WB55.
Pour cela nous utilisons l’application pour allumer ou éteindre la LED rouge du kit de développement.

* Pour cela appuyez sur le bouton menu ![image](images/app-menu.png):

![image](images/app7.png)

* Choisissez maintenant l’option ![image](images/app-switch.png):

![image](images/app8.png)

Vous pouvez sur cet écran piloter la LED Rouge du kit de développement.

