---
title: Glossaire
description: Glossaire

---
# Glossaire

**GPIO** : General Purpose Input Output (fr : Port D’entrée Sortie).

**LED** : Luminescent Electronic Diode_(fr: Diode ÉlectroLuminescente) .

**ADC** : Analog to Digital Converter (fr : Convertisseur Analogique vers Numérique).

**DAC** : Digital to Analog Converter (fr : Convertisseur Numérique vers Analogique).

**TIM** : Timer (fr : Horloge).

**UART** : Universal Asynchronous Receiver Transmitter  (fr: Récepteur Transmetteur asynchrone universel) 

**I2C** : Inter-Integrated Circuit(fr :_bus de communication_ intégré inter circuit)

**LCD** : Liquid Crystal Display(fr: écran à cristaux liquide)

**OLED** : Organic Luminescent Electronic Diode (fr: Diode ÉlectroLuminescente Organique) .

**µC** : Microcontrôleur.

**ALU** : Arithmetic and Logic Unit (fr : Unité d’opération arithmétique et logique)

**BLE** : Bluetooth Low Energy (fr : Bluetooth faible consommation énergétique).

**Cortex M4** : Type de coeur d’un microcontrôleur développé par la société ARM.

**RAM** : Type de mémoire volatile dans laquelle sont placées les variables. 

**FLASH** :Type de mémoire non volatile dans laquelle est placé le code compilé.

**ST-LINK V2** : Programmeur ST pour transférer le code dans la mémoire FLASH et RAM d’un microcontrôleur.

**NUCLEO** : Nom commercial donné aux cartes de développement fabriquées par STMicroelectronics.

**Morpho** : Type de connecteur ARDUINO.

**Arduino** : Écosystème éducatif de cartes électroniques pouvant s’interconnecter.

**Firmware** : Nom donné au fichier de sortie d’un programme compilé.

**Bootloader** : Système de démarrage permettant la mise à jour du firmware.

**DFU** : Device Firmware Update(fr: Mise à jour firmware du système).