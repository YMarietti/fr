---
title: Tutoriel Alarme
---

# Tutoriel de programmation d'une alarme en MicroPython avec une carte STM32WB55

Ce tutoriel est un exemple de potentiel Tp ou exercice à réaliser. Nous utiliserons en parallèle le *buzzer* ainsi que que le détecteur de mouvement (PIR motion sensor).
Le but est simple : si un mouvement est détecté, le buzzer produit un son.

- **Prérequis :**

Se reporter aux tutoriels du buzzer et du capteur de mouvement pour plus de détails.

Nous brancherons cependant le buzzer sur la **broche D4** et le capteur de mouvement sur la **broche D2**.

- **Programmation d'une alarme :**

Si un mouvement est détecté, l'interruption se déclenche ce qui provoque l'activation de la led et du buzzer.


Code MicroPython :
```python
from machine import Pin
from time import sleep
from pyb import Pin, Timer

motion = False

frequency = 440

BUZZER = Pin('D4') # D4 has TIM3, CH2
tim3 = Timer(3, freq=frequency)
ch2 = tim3.channel(2, Timer.PWM, pin=BUZZER)

def handle_interrupt(pin):
  global motion
  motion = True
  global interrupt_pin
  interrupt_pin = pin

led = Pin('LED', Pin.OUT)
pir = Pin('D2', Pin.IN)

pir.irq(trigger=Pin.IRQ_RISING, handler=handle_interrupt)

while True:
  if motion:
    print('Motion detected!')
    #print('Motion detected! Interrupt caused by:', interrupt_pin)
    led.value(1)
    ch2.pulse_width_percent(5)
    sleep(1)
    led.value(0)
    ch2.pulse_width_percent(0)
    print('Motion stopped!')
    motion = False

```

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
