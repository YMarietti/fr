---
title: Tutoriel Sound-Sensor
---

# Tutoriel d'utilisation du capteur Sound-sensor en MicroPython avec une carte STM32WB55

- **Prérequis :**

Le kit de capteurs fournit un *shield* (image ci-dessous) qui s’adapte sur la carte et simplifie la connexion des capteurs.
Il suffit de le connecter à la carte.
Brancher ce capteur au shield sur la broche D4. Les broches Dx permettent de traiter un signal digital (0 ou 1) or les broches Ax gèrent les signaux analogiques.

![Image](images/shield.png)
![Image](images/shield_carte.png)


Pour écrire et exécuter un script MicroPython, rendez vous dans le fichier main.py se trouvant dans le lecteur PYBFLASH de votre carte :

![Image](images/pybflash.png)

Le fichier main.py sera exécuté par défaut au démarrage de MicroPython. Ouvrez ce fichier avec un éditeur de script (ex: Putty).


Pour ce capteur il faudra le brancher sur la **broche A1**.

- **Capteur de son :**

![Image](images/capteur_sonore.png)


Ce capteur peut être utilisé comme détecteur de bruit, en effet sa sortie est proportionnelle au niveau sonore environnant.

Code MicroPython :
```python
import pyb
import time
from pyb import Pin

adc=ADC(PIN('A1'))

while True :
    time_sleep_ms(500)
    print(adc.read())

```

Une valeur sera affichiée dans l'interpréteur Python toutes les 0.5s.

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
