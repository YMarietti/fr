---
title: Tutoriel LED Infrarouge
---

# Tutoriel d'utilisation d'une LED infrarouge en MicroPython avec une carte STM32WB55

Ce tuto fonctionne pour tout type de LED mais nous prenons ici l'exemple d'une LED infrarouge.

- **Prérequis :**

Le kit de capteurs fournit un *shield* (image ci-dessous) qui s’adapte sur la carte et simplifie la connexion des capteurs.
Il suffit de le connecter à la carte.
Brancher la LED au shield sur la broche D4. Les broches Dx permettent de traiter un signal digital (0 ou 1) or les broches Ax gèrent les signaux analogiques.

![Image](images/shield.png)
![Image](images/shield_carte.png)


Pour écrire et exécuter un script MicroPython, rendez vous dans le fichier main.py se trouvant dans le lecteur PYBFLASH de votre carte :

![Image](images/pybflash.png)

Le fichier main.py sera exécuté par défaut au démarrage de MicroPython. Ouvrez ce fichier avec un éditeur de script (ex: Putty).

- **Faire clignoter la LED IR :**


Le code proposé est très simple et consiste à faire clignoter notre LED en boucle après une initialisation de la broche D4.

*Remarque*: Si la LED utilisée est une LED infrarouge, il est possible de vérifier qu'elle clignote bien avec la caméra de votre téléphone !

Code MicroPython :
```python
import pyb
from pyb import Pin
import time

led = Pin('D4', Pin.OUT_PP)

while True :
	time.sleep_ms(500)      # sleep for 500 milliseconds
	led.off()
	print("LED OFF")
	time.sleep(1)           # sleep for 1 second
	led.on()
	print("LED ON")
```

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
