---
title: Exercice avec le capteur de distance par ultrason Grove en MicroPython
description: Exercice avec le capteur de distance par ultrason Grove en MicroPython
---

# Exercice avec le capteur de distance par ultrason Grove en MicroPython

<div align="center">
<img alt="Grove - GPS" src="images/grove-ultrason.jpg" width="400px">
</div>

Bientôt disponible

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
