---
title: Exercice avec le module GPS SIM28 Grove en MicroPython
description: Exercice avec le module GPS SIM28 Grove en MicroPython
---

# Exercice avec le module GPS SIM28 Grove en MicroPython

<div align="center">
<img alt="Grove - GPS" src="images/grove-gps.jpg" width="400px">
</div>

Bientôt disponible

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
