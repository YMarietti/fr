---
title: Exercices MicroPython avec les capteurs Grove
description: Exercices MicroPython avec les capteurs Grove

---
# Exercices MicroPython avec les capteurs Grove

Vous devrez disposez de la carte d’extension Grove pour réaliser ces exercices.

![Image](images/shield.png)
![Image](images/shield_carte.png)

## Démarrage

La carte d’extension Grove est une carte au format Arduino permettant de brancher des capteurs et des actionneurs digitaux, analogiques, UART et I2C suivant la connectique [Grove](http://wiki.seeedstudio.com/Grove_System/).

## Exercices avec les capteurs Grove

Vous trouverez ici quelques exercices pour les [principaux capteurs au format Grove](http://wiki.seeedstudio.com/Grove_System/).

 - [DEL Infrarouge](del_ir)
 - [Potentiométre](potentiometre)
 - [Luminosité](luninosite)
 - [Afficheur 7 segments](afficheur7)
 - [Alarme](alarme)
 - [Buzzer](buzzer)
 - [Mouvement](mouvement)
 - [Joystick](joystick)
 - [Bruit](bruit)
 - [Choc (Tilt)](choc)
 - [Toucher](toucher)
 - [Afficheur LCD 16 caractères x 2 lignes](lcd_16x2) (bientôt)
 - [Distance par Ultrason](distance_ultrason) (bientôt)
 - [Module UART GPS SIM28](gps) (bientôt)
 - [Nintendo Nunchuk](nunchuk) (bientôt)
 - [Sonde étanche de température DS1820](ds1820)

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
