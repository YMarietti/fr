---
title: Tutoriel Joystick
---

# Tutoriel d'utilisation du capteur joystick en MicroPython avec une carte STM32WB55

Le kit de capteurs fournit un *shield* (image ci-dessous) qui s’adapte sur la carte et simplifie la connexion des capteurs.
Les broches Dx permettent de traiter un signal digital (0 ou 1) or les broches Ax gèrent les signaux analogiques.

![Image](images/shield.png) 
![Image](images/shield_carte.png)


Pour écrire et exécuter un script MicroPython, rendez vous dans le fichier main.py se trouvant dans le lecteur PYBFLASH de votre carte :

![Image](images/pybflash.png)

Le fichier main.py sera exécuté par défaut au démarrage de MicroPython. Ouvrez ce fichier avec un éditeur de script (ex: Putty).

Pour ce capteur il faudra le brancher sur la **broche A0**.

- **Le thumb-joystick :**


![Image](images/joystick.png)


Ce joystick est similaire à ceux que l'on peut retrouver sur une manette de PS2. Chacun des axes est relié à un potentiomètre  qui va fournir une tension correspondante. De plus le joystick possède un bouton poussoir.
Le code permet de vérifier tous les états que le joystick peut prendre. On pourra vérifier nos input dans l'interpréteur Python.


Code MicroPython :
```python
import pyb
import time
from pyb import Pin

vertical = ADC(PIN('A0'))            #en branchant le joystick sur A0,l'axe Y sera lu par A0 et l'axe X par A1
horizontal= ADC(PIN('A1'))           #sur A1, Y sera lu par A1 et X par A2; sur A2, Y sera lu par A2 et X par A3..

while True :
    time_sleep_ms(500)
    x=vertical.read()
    y=horizontal.read()

    if (x<=780 && x>=750) :
        print("Haut")
    if (x<=280 && x>=240) :
        print("Bas")
    if (y<=780 && y>=750) :
        print("Gauche")
    if (y<=280 && y>=240) :
        print("Droite")
    if (x>=1000) :                #en appuyant sur le joystick, la sortie de l'axe X se met à 1024, le maximum.
        print("Appuyé")
```

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
