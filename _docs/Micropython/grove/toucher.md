---
title: Tutoriel Touch Sensor
---

# Tutoriel d'utilisation du capteur touch sensor en MicroPython avec une carte STM32WB55

- **Prérequis :**

Le kit de capteurs fournit un *shield* (image ci-dessous) qui s’adapte sur la carte et simplifie la connexion des capteurs.
Il suffit de le connecter à la carte.
Brancher ce capteur au shield sur la broche D4. Les broches Dx permettent de traiter un signal digital (0 ou 1) or les broches Ax gèrent les signaux analogiques.

![Image](images/shield.png)
![Image](images/shield_carte.png)


Pour écrire et exécuter un script MicroPython, rendez vous dans le fichier main.py se trouvant dans le lecteur PYBFLASH de votre carte :

![Image](images/pybflash.png)

Le fichier main.py sera exécuté par défaut au démarrage de MicroPython. Ouvrez ce fichier avec un éditeur de script (ex: Putty).


- **Capteur tactile (Touch sensor):**

Ce capteur ne possède que 3 broches qu'il faut connecter à GND et VCC (alimentation, câble rouge et noir) et D4(signal, câble jaune).

![Image](images/capteur_tactile.png)

Ce capteur se comporte comme un interrupteur; il sera dans l’état *ON* s’il y a contact et *OFF* dans le cas contraire.

Voici le code en MicroPython:
```python

import pyb
import time
from pyb import Pin

p_in = Pin('D4', Pin.IN, Pin.PULL_UP)


while True :
	time.sleep_ms(500)      # sleep for 500 milliseconds

	print(p_in.value()) # get value, 0 or 1

	if(p_in.value() == 1):
		print("ON")
	else:
		print("OFF")
```
