---
title: µPython et STM32WB
description: µPython et STM32WB

---
# MicroPython et STM32WB

<p>
<img align="center" src="images/upython.png" alt="upython" width="100"/>
<img align="center" src="images/st2.png" alt="stm" width="250"/>
</p>

Vous trouverez dans cette partie tous les tutoriels MicroPython.

Avant toute chose, assurez vous d'avoir une installation fonctionnelle.
Le protocole à suivre est détaillé dans les 2 guides de démarrage.

## Sommaire

* [Qu'est ce que MicroPython ?](micropython)
* [Guide de démarrage rapide Windows 10](install_win10)
* [Guide de démarrage rapide Linux](install_linux)
* [Le kit de développement STM32WB55](stm32wb55)
* [Exemples basique d'utilisation de MicroPython](exercices)
* [Exemples d'utilisation de MicroPython avec la carte d'extension IKS01A3](iks01a3)
* [Exemples d'utilisation de MicroPython avec les capteurs Grove](grove)
* [Utilisation du BLE avec un smartphone](ble)
* [Création d'une application Smartphone avec MIT App Inventor](AppInventor)
* Programmation d'un mini-robot et contrôle de ses moteurs DC depuis un Smartphone (bientôt)
* [Foire aux Questions](faq)
* [Glossaire](glossaire)
* [Liens Utiles](ressources)
