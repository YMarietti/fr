---
title: Le kit de développement STM32WB55
description: Le kit de développement STM32WB55

---
# Le kit de développement STM32WB55

## Aperçu de la carte Nucleo STM32WB55

La carte STM32-NUCLEO est conçue avec les composants nécessaires au démarrage du microcontrôleur.

Les connecteurs d’extensions (ARDUINO, MORPHO) permettent à l'utilisateur de connecter des composants électroniques externes au STM32.

La carte électronique dispose aussi d’un connecteur pour pile CR2032, de 3 boutons (SW1,SW2,SW3), de 3 LEDs (rouge, vert et bleu) et de 2 connecteurs micro-USB.

_Voici le schéma bloc du kit NUCLEO-WB55:_
 
![image](images/architecture.png)

## Description de la carte Nucleo STM32BW55

_Vue de dessus et dessous du kit de développement:_

![image](images/board.png)


### PCB Antenna

C’est l’antenne bluetooth du microcontrôleur STM32WB55. C’est une antenne dite PCB car elle est intégrée directement au circuit de la board. Sa forme complexe est conçue de sorte à optimiser la réception et l’émission des ondes radio pour la fréquence du bluetooth soit 2,4 GHz. 


### Connecteurs Arduino / Morpho 

Des kits d’extensions peuvent être ajoutés simplement grâce à ces connecteurs normalisés. 

On pourra,  par exemple, brancher le kit Mems Microphone (X-NUCLEO-CCA02M2) sur la carte NUCLEO-WB55 :

![image](images/boards.png)


### USB User

C’est le port USB qui nous servira à:

*   Communiquer avec l'interpréteur MycroPython. 
*   Programmer le système.
*   Alimenter le kit de développement


### Socket CR2032

Une fois le système programmé, il sera possible d’alimenter le kit par une Pile CR2032, afin de rendre le système portable.


#### STLINK

C’est l’outil qui permet de programmer le microcontrôleur, pour cela utiliser le "glissé déposé" pour déposer le firmware sur le périphérique USB : **WB55-NODE**. 

### User LEDs

Ces LEDs sont accessibles au développeur et permettent d’être activées grâce à MicroPython. 

Voir l'exemple changer l’état d’une Sortie (GPIO) (Allumer une LED)

