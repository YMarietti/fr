---
title: Comment utiliser le kit STM32 MicroPython au moyen de RIOT OS ?
description: Comment utiliser le kit STM32 MicroPython au moyen de RIOT OS
---
# Comment utiliser le kit STM32 MicroPython au moyen de RIOT OS ?

## RIOT

[RIOT OS](http://riot-os.org/) est un système d'exploitation temps-réel pour les micro-contrôleurs constraints en mémoire et en énergie. RIOT OS supporte un grand nombre de cartes STM32 Nucleo et Discovery. Les programmes peuvent être développés en C, Lua, Javascript et MicroPython.

## Sommaire

Vous trouverez dans cette partie des exercices pour programmer en MicroPython avec RIOT OS sur le kit pédagogique Stm32Python.

Avant toute chose, assurez vous d'avoir une installation fonctionnelle. Le protocole à suivre est dans la section [Installation](installation).

 - [Installation](installation)
 - [Exercices](exercices)
