---
title: RIOT
description: Comment installer MicroPython sur RIOT OS
---
# Installation de MicroPython sur RIOT OS

**Outils nécessaires à l'installation de MicroPython :**  
Il est nécessaire d'utiliser un __ordinateur Windows avec une machine virtuelle Linux__ installée ou un _ordinateur Linux directement_.

## Installation de MicroPython RIOT pour STM32 sur un ordinateur Linux  

Depuis le bureau Linux, ouvrez un terminal en faisant
Clic Droit puis « Ouvrir un terminal ici »  
Entrez ensuite les commandes suivantes une par une
afin d’installer les logiciels pré-requis.  
```
sudo apt-get install git
sudo apt-get install make
sudo apt-get install gcc
sudo apt-get install gcc-arm-none-eabi
sudo apt-get install cmake
sudo apt-get install lisusb-1.0
```
C'est necessaire aussi d'intaller l'outil __ST-LINK-UTILITY__
```
git clone https://github.com/texane/stlink
cd stlink
make release
cd build/Release
sudo make install
cd
sudo ldconfig
```
Pour vérifier si l'outil est bien installé, vous pouvez utiliser le commande __*`st-info`*__


Une fois les logiciels pré-requis installées, il est nécessaire de récupérer le projet MicroPython sur RIOT depuis l’outil git en écrivant dans un terminal (ouvert depuis le bureau comme précédemment) les commandes suivantes:

```
git clone https://github.com/RIOT-OS/micropython
```
Avant de construire le firmware pour une carte donnée, le compilateur croisé MicroPython doit être construit.
```
cd micropython
make -C mpy-cross
```
Les sous-modules doivent d'abord être obtenus en utilisant:

```
cd ports/stm32
make submodules
```
Ensuite, pour construire pour un carte:
```
make BOARD={Your-Board-Name}
```
Il est nécessaire de remplacer __{Your-Board-Name}__ par le nom de la carte STM32 utilisée.
Par exemple, si vous utilisez un *NUCLEO F446RE*, il sera nécessaire d’écrire la commande :  
__`make BOARD=NUCLEO_F446RE`__

Vous pouvez verifier si votre carte est compatible [ici](https://github.com/RIOT-OS/micropython/tree/master/ports/stm32/boards)

Où sur le terminal, dans le repertoire *__micropython/ports/stm32/boards__*

**Portage de Micropython sur la carte**

Il est nécessaire de mettre la carte STM32 dans le __mode DFU__ (*bootloader*). Normalement, il faut connecter les pins 3v3 et BOOT0. L'image ilustre comme le faire pour la carte STM32F446RE.

![Image](images/Cablage_bootloader_stm32f446.png)

Toujours dans le repertoire __*ports/stm32*__, on peut flasher le micropython sur la carte avec le commande:
```
make BOARD={Your-Board-Name} deploy-stlink
```
Le programme *st-flash* devrait détecter automatiquement la connexion USB à la carte. Si ce n'est pas le cas, exécutez __lsusb__ pour déterminer son bus USB et son numéro de périphérique et définissez la variable d'environnement __STLINK_DEVICE__, en utilisant le format __<USB_BUS>: <USB_ADDR>__, comme dans l'example:
```
lsusb
[...]
Bus 002 Device 035: ID 0483:3748 STMicroelectronics ST-LINK/V2
export STLINK_DEVICE="002:0035"
make BOARD=NUCLEOF446RE deploy-stlink
```
 Si tout est bien passée, une message doit s'afficher.
```
2020-02-05T10:27:32 INFO common.c: Starting verification of write complete
2020-02-05T10:27:35 INFO common.c: Flash written and verified! jolly good!
```
Maintenaint, le micropython est dans votre carte! Vous pouvez ouvrir un terminal, dans une autre SE ei vous voulez, comme PuTTY, et si vous appuyez sur le button *Restart* de la carte, ou *__CTRL+D__* sur le terminal, il va vous afficher la message suivante:


![Image](images/micropython_riot_putty.png)
