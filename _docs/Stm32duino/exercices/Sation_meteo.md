---
title: Station météo avec STM32duino
description: Développer une station météo connectée en C/C++ avec STM32duino
---

# Développer une station météo en C/C++ avec STM32duino

<div align="justifiy">
Ce tutoriel a pour objectif de vous fournir un exemple de sketch STM32duino raisonnablement complet pour une station météo connectée.
</div>


*Bientôt disponible*
