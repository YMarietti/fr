---
title: Exercice avec le capteur de choc (tilt) en C/C++ pour Stm32duino
description: Exercice avec le capteur de choc (tilt) en C/C++ pour Stm32duino
---
# Exercice avec le capteur de choc (tilt) en C/C++ pour Stm32duino

- **Prérequis :**

Le kit de capteur fournis un *shield* (image ci-dessous). Il servira à connecter un capteur à la carte.
Il suffit de le connecter à la carte.
Brancher ce capteur au shield sur le pin D4. Les pins Dx permettent de traiter un signal digital (0 ou 1) et les pins Ax gèrent les signaux analogiques.

![Image](images/4_tilt_sensor/shield.png)        ![Image](images/4_tilt_sensor/shield_carte.png)  

Ouvrez Arduino et vérifiez que le port est connecté: Outils/Port, COM3 devrait être sélectionné.
- **Capteur Tilt-sensor :**

![Image](images/4_tilt_sensor/tiltsensorim.png)

Le tilt-sensor, capteur d'inclinaison en français, est un capteur qui mesure la position d'inclinaison par rapport à la gravité. La bille dans le capteur (influencé par le mouvement du capteur) roule et vient faire contact.

*Voici le code sur Arduino*
```c
void setup() {
  Serial.begin(9600); // initialisation de la connexion série
  pinMode(D4,INPUT);
}

void loop() {
  boolean etatContact=digitalRead(D4);
  if (etatContact)
    Serial.println("Contact");
  else
    Serial.println("Pas de contact");
}
```

Vérifiez et téléversez.
Pour regarder l'état dans lequel se trouve le capteur cliquez sur le *moniteur série*

![Image](images/4_tilt_sensor/moniteur_serie.png)

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
