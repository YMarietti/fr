---
title: Exercice avec la DEL Grove en C/C++ pour Stm32duino
description: Exercice avec la DEL Grove en C/C++ pour Stm32duino
---
# Exercice avec la DEL Grove en C/C++ pour Stm32duino

- **Prérequis :**

*Se reporter au tutoriel du tilt-sensor*: on branchera la led sur le pin D4.


![Image](images/5_LED_externe/led.png)  


Le code proposé est très simple et consiste à faire clignoter notre led en boucle après une initialisation du pin.


*Voici le code sur Arduino*
```c
void setup() {
  Serial.begin(9600);
  pinMode(D4,OUTPUT);
}

void loop() {
  digitalWrite(D4,HIGH);    // on alume
  delay(1000);              // plus la valeur du paramètre est grande plus la led clignote lentement
  digitalWrite(D4,LOW);     // on éteind
  delay(1000);
}
```

*Remarque*: Si la led utilisé est une led infrarouge, il est possible de vérifier clignote bien avec la caméra de votre téléphone !

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
