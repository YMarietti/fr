---
title: Exercices avec Stm32duino en C/C++
description: Exercices avec Stm32duino en C/C++

---
# Exercices avec Stm32duino en C/C++

Voici la liste des exercices avec Stm32duino en C/C++ :

- [DEL](del_blink)
- [Capteur Grove](grove)
- [Choc](choc)
- [DEL - Suite](del)
- [Luminosité](luminosite)
- [Potentiometre](potentiometre)
- [Température](temperature)
- [Joystick](joystick)
- [Toucher](toucher)
- [Distance par Ultrason](ultrason)
- [Buzzer](buzzer)
- [Bruit](bruit)
- [Carte d’extension MEMS IKS01A1](iks01a1)
- [Carte d’extension MEMS IKS01A3](iks01a3)
- [Carte GNSS X-NUCLEO-GNSS1A1](x-nucleo-gnss1a1)
- [Carte de communication LoRa I-NUCLEO-LRWAN1](i-nucleo-lrwan1)
- [Communication avec Firmata](firmata)
- [Pilotage de moteurs avec la carte SparkFun Monster Moto Shield](monster_moto)
- [Afficheur LCD 16 caractères x 2 lignes](lcd_16x2) (bientôt)
- [Module UART GPS SIM28](gps) (bientôt)
- [Nintendo Nunchuk](nunchuk) (bientôt)
- [Sonde étanche de température DS1820](ds1820) (bientôt)
