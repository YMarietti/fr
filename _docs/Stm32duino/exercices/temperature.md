---
title: Exercice avec le capteur de température Grove en C/C++ pour Stm32duino
description: Exercice avec le capteur de température Grove en C/C++ pour Stm32duino
---
# Exercice avec le capteur de température Grove en C/C++ pour Stm32duino

De la même manière que pour les autres capteurs, nous utiliserons le shield que nous brancherons sur la carte.
Ouvrez Arduino et vérifiez que le port est connecté: Outils/Port, COM3 devrait être sélectionné.

- **Capteur de température :** *- Temperature sensor*

![Image](images/8_temperature/capteur_temp.png)

Ce capteur de température utilise une thermistance (résistance qui diminue avec la chaleur) pour mesurer la température ambiante. La résistance modifie la sortie d'un diviseur de tension qui est mesurée par une broche d'entrée analogique. Cette valeur devra être convertie en une valeur de température dans notre programme.     
La plage de fonctionnement est de -40 à 125 ° C, avec une précision de 1,5 ° C.

Le capteur doit être connecté au pin A0

*Voici le code sur Arduino IDE*
```c
//définition des constantes necessaires pour la conversion.
float R1 = 10000;
float logR2, R2, T;
float c1 = 0.001129148, c2 = 0.000234125, c3 = 0.0000000876741; //coefficients de Steinhart-Hart pour la conversion

void setup() {
  Serial.begin(9600);
  pinMode(A0,INPUT);                                  // on met le capteur en entrée analogique
}

void loop() {
  int Vo = analogRead(A0);
  R2 = R1 * (1023.0 / (float)Vo - 1.0);               //calcule la résistance du thermistor
  logR2 = log(R2);                                    //on a converti Vo en float plutôt que int pour le calcul
  T = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2)); // met la temperature en Kelvin.
  T = T - 273.15;                                     // converti des Kelvins aux Celcius.
 // T = (T * 9.0)/ 5.0 + 32.0;                        // converti des Celsius aux Fahrenheits.

  Serial.print("Temperature: ");
  Serial.print(T);
  //Serial.println(" K");
  Serial.println(" C");
  //Serial.println(" F");

  delay(500);
}
```

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
