---
title: Stm32duino
description: Section Stm32duino

---
# Stm32duino

[Stm32duino](https://github.com/stm32duino) est un projet rassemblant des bibliothèques Arduino pour les cartes de développement STM32 (Nucleo et Discovery) et pour les composants MEMS de ST Microelectronics. Il permet de développer et compiler des programmes (ie. _sketch_) depuis l'environnement de développement Arduino IDE.

# Sommaire

Vous trouverez dans cette partie tous les exercices [Stm32duino](https://github.com/stm32duino) pour le kit pédagogique Stm32Python.

Avant toute chose, assurez vous d'avoir une installation fonctionnelle.
Le protocole à suivre pour ce faire est détaillé dans la section [Installation](installation).

 - [Installation](installation)
 - [Exercices](exercices)
