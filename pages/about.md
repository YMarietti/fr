---
layout: page
title: A propos
permalink: /about/
---
# A propos

## Contexte

La réforme des lycées introduit un nouvel enseignement suivi par tous les élèves de seconde générale et technologique : [SNT (Sciences Numériques et Technologie)](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/08/5/spe641_annexe_1063085.pdf). Un des thèmes abordés par cet enseignement est l’Internet des objets (IdO, en anglais Internet of things, IoT) qui représente l’extension d’Internet à des choses et à des lieux du monde physique.

L’objectif est d’amener ces jeunes à un premier niveau de compréhension de l’internet des objets. L’enjeu est de favoriser une orientation choisie, en l’occurrence ici vers l’ingénierie du numérique. La part du « numérique » et de « l’informatique » dans les enseignements a été fortement augmentée avec la réforme du lycée.

## Objectif

L'objectif de STM32Python est de fournir aux enseignants du lycée et aux lycéens des supports pédagogiques open-source pour l'initiation à l’Internet des Objets pour l’enseignement de [SNT (Sciences Numériques et Technologie)](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/08/5/spe641_annexe_1063085.pdf). Ces supports s'appuient sur la plateforme Nucleo STM32 de ST Microelectronics. Ils permettent de réaliser des montages électroniques et des programmes pour les microcontroleurs STM32 avec les langages C/C++ et microPython.

Les supports réalisés sont également utilisables par d’autres enseignements de première et terminale générales, notamment en spécialité NSI (Numérique et Sciences Informatiques), en spécialité SI (Sciences de l’ingénieur), ou en série technologique STI2D (Sciences et Technologies de l’Industrie et du Développement Durable).


## Partenaires
Les partenaires de STM32Python sont:
* les rectorats des académies de [Grenoble](http://www.ac-grenoble.fr) et d’[Aix-Marseille](http://www.ac-aix-marseille.fr),
* [ST Microelectronics](https://www.st.com),
* [Inventhys](http://www.inventhys.com),
* [Polytech Grenoble](https://www.polytech-grenoble.fr), [Grenoble INP Institut d'ingénierie et de management](https://www.grenoble-inp.fr/), [Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr).

## Comment contribuer ?

Enseignant, Etudiant, Lycéen, Ingénieur, Hobbyiste, vous avez réalisé un complément à ces tutoriels : n'hésitez pas nous contacter pour contribuer au projet !

stm32python-contact@imag.fr

## Contributeurs
* Erwan LE SAINT
* Michael ESCODA
* Richard PHAN
* Romaric NOLLOT
* Guy CHATEIGNIER
* Didier DONSEZ
* Baptiste JOLAINE
* Aurélien REYNAUD
* Pedro LOPES
* Gaël LEMIERE
* Robin FARGES
* Florian VIOLET
* Leïla MICHELARD
* Manon CHAIX
* Gloria NGUENA